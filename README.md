# CSIS 10A - Assignment 15

__Prerequisite: Lecture or Reading on Chapter 15__

Clone or download _assignment15_ to your workspace. Open `project.bluej` and begin the exercises below:

1. Follow the instructions in the `OOP.java` file to complete this problem
2. Extra Credit (__5 points!!__) Complete the problems in `Inheritance.java`.

